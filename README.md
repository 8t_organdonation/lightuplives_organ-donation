# Lighting Up the Lives Organ Donation Campaign

The Lighting Up the Lives Campaign website aims to get people sign up for organ donation.
In terms of acheiving the goal, we have a number of sections where we show users why they should be registered as a doner.

## Team

- Jeongyun Cho (Front-End Developer)
- Changseok Lee (Back-End Developer)
- Jeril Kadavan (Web Designer)
- Reece Devine (Motion Designer / Project Manager)



## Landing Page

Landing page has video and text to convince user to be a doner. On campaign Section, We explain about our SNS campaign.
Which users can show their support and others can feel the same way and change their mind on being a donor.

![image](images/readme/main.png)
## Login / Create

Create Page and Login page share the same theme and user need to login to write comment on Article Page.

![image](images/readme/login.png)
![image](images/readme/create.png)

## Articles Page

Article page shows Organ Donation related news and if admin logged in and come to this page they can create article.

![image](images/readme/articles.png)

When go in to actual Aricle, comment section shows up on the bottom and user can write comment if they are registered and logged in.

![image](images/readme/article.png)

## Statistics Page

Statistics shows myth and fact about the organ donation in infographic style. There is an interview video which basically ask people how they think about organ donation.

![image](images/readme/statistics.png)

## SignUp/Contact Page

Sign Up section again leads user to be a doner and on contact section, user can write an mail to us.

![image](images/readme/signup.png)



## Built With:

- Adobe Illustrator, Photoshop, XD, cinema4D
- PHP/MySQL
- Gulp.js
- SASS
- Javascript Fetch API, to get data from backend database
- Vue.js
- Waypoints
- Bootstrap4
- curator.io
- GreenSock

## Acknowledgments

- pinterest (https://www.pinterest.ca/)
- awwwards (https://www.awwwards.com/)
- https://stackoverflow.com/
- https://vuejs.org/
- https://router.vuejs.org/

## Future Improvements

- Build edit, remove user and update Backend.
- Further refactoring SASS structure.
- Designs of few pages can be improved
