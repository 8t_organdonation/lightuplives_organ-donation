export default {
    template: `

   <div>
    <section id="signUp" class="container-fluid">
    <div class="row">
    
        <div class="col-12 col-md-7">
    <div id="signupH" class="col-8 col-md-6">
    
    <h2 id="signUpTag" class="text-center">SIGN UP</h2>

    </div>

    <div id="promoTxt" class="col-12 col-md-12 text-left">
        <h2>Every <span id="emphasis">30 hours</span> ,one person in 
        Canada dies waiting 
        for a transplant.</h2>

        <div id="promoP">

            <p>Registering to donate your tissues and organs can be the life-giving process to many people in Canada. This token of gift could save up to 8 lives, a day.</p>

            <p>Take an opportunity to donate someone another chance at life. One day that
            someone may be a friend, a close relative or a loved one. Nothing could be more generous than to bring beautiful light into someone’s life.</p>

            <p>The mission of our campaign is to put an end to the organ transplant waiting list. We aim to take this forward by increasing donor registration, raising awareness, and supporting families on the waitlist in their search for a living donor.</p>

        </div>
    
    </div>
</div>
    
<div class="col-12 col-md-5">
    <div class="col-12 map" id="map">
    <div class="imgContainer">
        <img class="map" src="images/web/page 6/map-canada.svg" alt="canada map svg">
        <p id="cTime"></p>
    </div>
    </div>
    <div class="col-12 col-md-8 mx-auto">
    <button class="btn draw-border"><span class="bold">REGISTER</span><br> with Service Ontario</button>
    </div>
</div>
</div>
</section>

    <div class="row">
    <div class="cont-contactBtn">
    <section id="conTact" class="cont-flip mx-auto">
        <h2 class="hide">Contact Section</h2>
        <div id="frontCard" class="front">
            <a @click.prevent="toggleFlip()" href="#" class="btn btnn flip" ><P id="sendBut">Send Message</P></a>
        </div>
        <div class="back">
            <div class="col-8 col-md-12 mx-auto align-self-center">
                <h2 id="conTactTag" class="text-center">Contact Us</h2>
            </div>

            <form class="contact-form" action="">
            <label name="name" for="name" class="hide">Name</label>
            <input v-model="input.name" name="name" class="gutter" type="text" placeholder="Name">

            <label name="email" for="email" class="hide">Email</label>
            <input v-model="input.email" name="email" class="gutter" type="text" placeholder="Email">

            <textarea v-model="input.message" rows="6" name="message" id="" placeholder="Leave a message"></textarea>
            <button @click.prevent="send_mail()" class="commentBtn" type="submit">SEND</button>
            </form>
        </div>
    </section>
    </div>
    </div>
    </section>

   </div> 
   


    `,

    data() {
        return {
            input:{
                name:"",
                email:"",
                message:""
            }
        }
    },
      
    mounted() {
     

          function startTime() {
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('cTime').innerHTML =
            h + ":" + m + ":" + s;
            var t = setTimeout(startTime, 500);
          }
          function checkTime(i) {
            if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
            return i;
          }
        
          startTime();
}, methods: {
    toggleFlip(){
        var flip = document.querySelectorAll('.cont-flip')[0];
        flip.classList.toggle('flipped');
      },
    send_mail(){
            
            if (this.input.name !== "" && this.input.email && this.input.message !== "" ) {
            //   do fetch call
            // add to form data
              let formData = new FormData();
      
              formData.append("name", this.input.name);
              formData.append("email", this.input.email);
              formData.append("message", this.input.message);
         
             
              let url = `./admin/scripts/contact.php`;
            
              fetch(url, {
                method: "POST",
                body: formData
              })
                .then(res => res.json())
                .then(data => {
                  console.log(data);
                  if (data == "Mail has been Sent") {
                    this.$router.replace({ name: "main" }); 
                } else {
                    console.log("Creation failed, try again");
                    this.$emit("autherror", data);  
                  }
                })
                .catch(function (error) {
                  console.log(error);
                });
            } else {
              let warn = ("Fields shouldn't be blank");
              this.$emit("autherror", warn);  
            }
          
    }
}
}