export default {
    template: `
    <div>
    <div class="login-page">
    
    
    <div class="form">
    <p id="logTitle">CREATE ARTICLE</p>
  
    <form class="login-form" method="post" enctype="multipart/form-data">
        <label name="title" for="title" class="hide">Article Title</label>
        <input v-model="input.title" name="title" type="text" placeholder="title *" />
        
        <label name="subtitle" for="subtitle" class="hide">Article subTitle</label>
        <input v-model="input.subtitle" name="subtitle" type="text" placeholder="subtitle *" />
      
        <label for="articlePic">Article Image *</label>
        <input v-on:change="articleImgAdd" ref="articleImg" type="file" name="articleImg" id="articleImg"/>

        <label name="content" for="content" class="hide">Article Content</label>
        <input v-model="input.content" name="content" type="text" placeholder="content *" />
       
        <label name="source" for="source" class="hide">Article Source</label>
        <input v-model="input.source" name="source" type="text" placeholder="source *" />

        <label name="author" for="author" class="hide">Article Author</label>
        <input v-model="input.author" name="author" type="text" placeholder="author *" />

        <button type="submit" @click.prevent="create_user()">Create User</button>
        
    </form>
    
    </div>
</div>
</div>
     `,

     data() {
         return {
             input: {
              title: "",
              subtitle: "",
              articleImg: "",
              content: "",
              source: "",
              author: ""
             
             }
         

         }
     },
 
     methods: {
      articleImgAdd(){
        this.input.articleImg = this.$refs.articleImg.files[0]
      },
        create_user() {
     
            console.log(this.input.article);
            
            if (this.input.title !== "" && this.input.subtitle && this.input.articleImg !== "" && this.input.content !== "" && this.input.source !== "" && this.input.author !== "") {
            //   do fetch call
            // add to form data
              let formData = new FormData();
      
              formData.append("title", this.input.title);
              formData.append("subtitle", this.input.subtitle);
              formData.append("articleImg", this.input.articleImg);
              formData.append("content", this.input.content);
              formData.append("source", this.input.source);
              formData.append("author", this.input.author);
             
              let url = `./admin/scripts/admin_createarticle.php`;
            
              fetch(url, {
                method: "POST",
                body: formData
              })
                .then(res => res.json())
                .then(data => {
                  console.log(data);
                  if (data == "Article Create Failed") {
                    console.log("Creation failed, try again");
                    this.$emit("autherror", data);  
                } else {
                    this.$router.replace({ name: "news" });    
                  }
                })
                .catch(function (error) {
                  console.log(error);
                });
            } else {
              let warn = ("Fields shouldn't be blank");
              this.$emit("autherror", warn);  
            }
          }
    }
 }