export default {
    template: `
   <div>
   <div id="mainFact">
       <div id="chartLogo" class="mx-auto">
           <img id="logoText" src="images/chart_text.svg" alt="Logo Text">
           <img id="logoMain" src="images/chart_main.svg" alt="logoLetter">
           <img id="logoRibbon" src="images/ribbon.svg" alt="ribbon">
       </div>  
   </div>   
   <div id="mainFactText">  
       <div class="semiCircleEdge topEdgeRed"></div> 
       <div id="s1Parallax1" class="s1Parallax" data-speed="4"></div>
       <div id="s2Parallax1" class="s2Parallax" data-speed="-4"></div>
       <div id="checkText">
               <h2>Did you know that 1 donor can save 8 lives?</h2>
               <div id="vidPromo" class="col-12 col-md-6 mx-auto">
               <div class="embed-responsive embed-responsive-4by3">
                   <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/iBKkybXM1XA" allowfullscreen></iframe>
                 </div>
           </div>
               <h3>Check Out these Facts about Organ Donation</h3>
       </div>
       <div class="semiCircleEdge bottomEdgeAqua"></div> 
   </div>    
   <div id="misCon">
           <h3>Myths and Facts</h3>
           <h4>Click <img class="checkBtn" src="images/checked.svg" alt="checked"> to check the Facts!</h4>
           <div class="container">
               <div class="row">
                   <div id="card1" class="col-12 col-md-4 cards">
                           <img class="cardImages" src="images/teamwork.svg" alt="team work photo">
                           <img id="0" class="checkBtn" src="images/checked.svg" alt="checked">
                           <h4 class="factAnswer" data-text-swap="There's no age limit to organ donation. 
                           To date, the oldest donor in the U.S. was age 93. ">1. I'm too old to be a donor.</h4>
                   </div>
                   <div id="card2" class="col-12 col-md-4 cards">
                            <img class="cardImages" src="images/notes.svg" alt="cash photo">
                            <img id="1" class="checkBtn" src="images/checked.svg" alt="checked">
                            <h4 class="factAnswer" data-text-swap="There is no cost to donors or their families for organ or tissue donation.">2. My family will have to pay for the donation.</h4>
                   </div>
                   <div id="card3" class="col-12 col-md-4 cards">
                           <img class="cardImages" src="images/eight.svg" alt="eight photo">
                           <img id="2" class="checkBtn" src="images/checked.svg" alt="checked">
                           <h4 class="factAnswer" data-text-swap="One person can save eight lives and enhance 75 others through organ, eye and tissue donation.">3. Not many people can be saved with a donation.</h4>
                   </div>
               </div>
           </div>
   </div>
   <div id="situationCon">
       <div class="semiCircleEdge bottomEdgeRed"></div> 
       <p>How is the situation like?</p>
       <h3>Organ Donation Situation in Ontario,CA</h3>
       <div class="container">
           <div class="row">
               <div class="col-12 col-md-4"><h4>The need for organ donors in <span>Ontario:</span></h4></div>
               <div class="col-12 col-md-4">
                   <img class="situationImg" src="images/clipboard.svg" alt="register icon">
                   <h5>33%</h5>
                   <h6>of Ontarians are registered donors.</h6>
               </div>
               <div class="col-12 col-md-4">
                   <img class="situationImg" src="images/give.svg" alt="give icon">
                   <h5>17,639</h5>
                   <h6>Ontarians have received a lifesaving organ transplant since 2003.</h6>
               </div>
           </div>
           <div class="row">
                   <div class="col-12 col-md-6 mx-auto">
                       <p id="middleText">However,</p>
                       <div class="counters">
                               <h3 id="counterNum" class="counter centerText" aria-label="1,630">1,630</h3>
                       </div>
                       <h4 class="centerText">Ontarians are currently waiting for an organ transplant. As of December 31, 2018</h4>  
                   </div>
           </div>
       </div>
   </div>
   <div class="semiCircleEdge bottomEdgeGrey"></div>
   <div id="supportSection">
                   <h2>We Need Your Support</h2>
                   <h3>Show Your Support on Our SNS Campaign and also Sign Up to be a <span>Donor!</span>
                   <button id="supportBtn" class="btn draw-border"><span class="bold">REGISTER</span><br> with Service Ontario</button>
                   </h3>
   </div>

   </div> 

    `,

    data() {
        return {
            
        }
    },created(){
   
    },
    mounted() {
// TweenMax for Heart Logo
var mainFact = document.querySelector('#mainFact');
var logoMain = document.querySelector('#logoMain');

var waypoint = new Waypoint({
  element: mainFact,
  handler: function(direction) {


     TweenMax.from(logoMain, 0.375, {
      repeat: 6,
      yoyo: true,
      ease: Power1.easeIn,
      scale:1.1
    });
  
      this.destroy();
  },
  offset: 130
});


// parallax scroll
function parallax() {
  var slider = document.querySelector("#s1Parallax1");
  var slider2 = document.querySelector("#s2Parallax1");

	var yPos = window.pageYOffset / slider.dataset.speed;
  yPos = -yPos;
  
  var yPos2 = window.pageYOffset / slider2.dataset.speed;
	yPos = -yPos;
	
  var coords = '0% '+ yPos + 'px';
  var coords2 = '0% '+ yPos2 + 'px';
	
  slider.style.backgroundPosition = coords;
  slider2.style.backgroundPosition = coords2;
}

window.addEventListener("scroll", function(){
	parallax();	
});


// check answer function
var answers = document.querySelectorAll(".factAnswer");
var button = document.querySelectorAll(".checkBtn");

for(var i=0; i<button.length; i++){
  button[i].addEventListener('click', function(e) {
    var answer = answers[e.target.id];
    if (answer.getAttribute("data-text-swap") == answer.innerHTML) {
      answer.innerHTML = answer.getAttribute("data-text-original");
    } else {
      answer.setAttribute("data-text-original", answer.innerHTML);
      answer.innerHTML = answer.getAttribute("data-text-swap");
    }
  }, false);
}


  // number counter

const ready = (selector, callback) => {
	window.addEventListener('DOMContentLoaded', function () {
		const elems = [...document.querySelectorAll(selector)];
		if (elems.length) {
			for (let elem of elems) {
				callback(elem);
			}
		}
	});
};

ready('.counter', (stat) => {
	// pattern used to seperate input number from html into an array of numbers and non numbers.
	const patt = /(\D+)?(\d+)(\D+)?(\d+)?(\D+)?/;
	const time = 1000;
	let result = [...patt.exec(stat.textContent)];
	let fresh = true;
	let ticks;
	
	// Remove first full match from result array 
	result.shift();
	// Remove undefined values from result array 
	result = result.filter(res => res != null);

	while (stat.firstChild) {
		stat.removeChild(stat.firstChild);
	}

	for (let res of result) {
		if (isNaN(res)) {
			stat.insertAdjacentHTML('beforeend', `<span>${res}</span>`);
		}
		else {
			for (let i = 0; i < res.length; i++) {
				stat.insertAdjacentHTML('beforeend',
					`<span data-value="${res[i]}">
						<span>&ndash;</span>
						${Array(parseInt(res[i]) + 1).join(0).split(0).map((x, j) => `
							<span>${j}</span>
						`).join('')}
					</span>`
				);
			}
		}
	}


	ticks = [...stat.querySelectorAll('span[data-value]')];

	let activate = () => {
		let top = stat.getBoundingClientRect().top;
		let offset = (window.innerHeight * 3 / 4);

		setTimeout(() => {
			fresh = false;
		}, time);

		if (top < offset) {
			setTimeout(() => {
				for (let tick of ticks) {
					let dist = parseInt(tick.getAttribute('data-value')) + 1;
					tick.style.transform = `translateY(-${(dist) * 100}%)`
				}
			}, fresh ? time : 0);
			window.removeEventListener('scroll', activate);
		}
	}
	window.addEventListener('scroll', activate);
	activate();
});
}
}
