export default {
    template: `
    <div>
    <!-- intro -->
<section id="intro" class="container-fluid">

<h2 class="hide">Intro Section</h2>
<div class="row">
<div class="col-md-4">
    <div class="col-12">
        <h2 id="tagLine">LIGHTING UP </br>LIVES</h2>
    </div>
    <img class="pulse" src="images/web/page 1/pulse_1.svg" alt="Purse1 icon">
    <div class="col-8 mx-auto">
    <button class="btn draw-border"><span class="bold">REGISTER</span><br> with Service Ontario</button>
    </div>
</div>
<div class="col-md-8 align-self-center">
    <div id="vidPromo" class="col-12 col-md-10 mx-auto">
        <div class="embed-responsive embed-responsive-4by3">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/8R8jUx6mSEY" allowfullscreen></iframe>
          </div>
    </div>
</div>
</div>
</section>

    <!-- stages -->
<section id="stages" class="container-fluid">

    <h2 class="hide">Stage Section</h2>
    <div class="row">
    <div id="stgHead" class="col-8 col-md-4 col-lg-2 order-md-2 mx-auto align-self-center">
        <h2>Why </br>Become </br>Donor<span id="qMark">?</span></h2>
    </div>

        <div class="col-11 col-md-8 col-lg-10 order-md-1 spTxt">
        <div>
            <h3>01</h3>
            <p class="float-right float-sm-none">You can save as many as 50 lives by donating your organs.</p>
        </div>
               
     
         <div> 
            <h3>02</h3> <p class="float-right float-sm-none">Over 1,600 people in Canada are added to 
            organ wait lists every year.</p>
        </div>
            <div> 
            <h3>03</h3> <p class="float-right float-sm-none">Nothing better than being able to give a 
            healthy organ to improve the life of 
            another person.</p>
        </div>
            <div> 
            <h3>04</h3><p class="float-right float-sm-none">Less than 20% of people in Canada have 
            decided to become donors although most of 
            the people support the notion.</p>
        </div>
            <div> 
            <h3>05</h3><p class="float-right float-sm-none">There are only 18 donors per million people in 
            Canada and more than 4,500 Canadians 
            waiting for life-saving transplants.</p>
        </div>
            <div> 
            <h3>06</h3><p class="float-right float-sm-none">One donor can benefit 75 people in Canada 
            and Save up to 8 lives.</p>
        </div>
            <div> 
            <h3>07</h3><p class="float-right float-sm-none">Around 300 people in Canada die 
            every year waiting for an organ transplant</p>
            </div> 
    </div>
</div>
    <div class="rightF">
        <img class="pulse" src="images/web/page 2/pulse_2.svg" alt="pulse2 icon">

    </div>
</section>

<!-- about -->
<section id="about" class="container-fluid">
        <div class="row">

    <div class="col-10 col-md-3 mx-auto order-md-1 text-center">
        <h2 id="abTitle">About Campaign</h2>
       
    <div class="row d-none d-md-block">
        <!-- show <md -->
            <div class="col-12">
                    <img class="pulse" src="images/web/page 1/pulse_1.svg" alt="pulse3 icon">
                 </div>

            <div class="col-8 mx-auto">
                <button class="btn draw-border"><span class="bold">REGISTER</span><br> with Service Ontario</button>
            </div>
         </div>
    </div> 
    
    <div id="abText" class="col-12 col-md-5 offset-md-1 order-md-3 align-self-center">
            <p>Registering to be a potential organ donor may brighten up the hopes of countless people and families.</p>
            <p>Our aim is to promote awareness for the people in Ontario to come forward to register for the organ donation campaign. You can become an organ donor through simple steps as mentioned below.</p>
            <p>The steps follow by taking a photo with #LightUp8Lives on Twitter. All you have to do is capture a photo posing heart on heart. Choose the photo you like the most. Post it on Twitter with a #LightUp8Lives tag and it can be viewed on our website.</p>          
            <p>The utmost objective of this project is to attract youth's attention and to appeal them to register for being an organ donor.</p>
    </div>
<!-- mobile only -->
         <div class="col-8 text-center mx-auto d-block d-md-none">
                <button class="btn draw-border"><span class="bold">REGISTER</span><br> with Service Ontario</button>
        </div>
    
         <div class="col-12 d-block d-md-none">
                <img class="pulse" src="images/web/page 1/pulse_1.svg" alt="pulse3 icon">
         </div>
 

    <div id="organLight" class="col-md-3 order-md-2 d-none d-md-block">
            <img src="images/web/page 3/organ_lights.svg" alt="organ lights">
        </div>
 <!-- mobile only ends -->        

        </div>
</section>

<!-- steps -->
     <section id="steps" class="container-fluid">
        <div class="row">
     <div class="col-md-8 d-none d-md-block">
            <object data="images/5steps/steps_d.svg" type="image/svg+xml" id="steps2" class="svg-graphic"></object>
        </div>
    <div class="col-12 col-md-4 align-self-center">
        <h2 id="stTitle"><span id="qMark">5</span> STEPS TO JOIN CAMPAIGN</h2>           
    </div>
    
    <div class="col-12 d-block d-md-none">
            <object data="images/5steps/steps_m.svg" type="image/svg+xml" id="steps3" class="svg-graphic"></object>
            </div>
    </div>

</section>

<!-- tweet -->
<section class="tweet container-fluid">
        <div class="row">
    <h2 class="hide">Twitter Hashtag Section</h2>

    <div class="col-12">
        <h2>#LightUp8Lives</h2>
  <!-- curator-feed need to be included on free version -->

    <div id="curator-feed"><a href="https://curator.io" target="_blank" class="crt-logo crt-tag">Powered by Curator.io</a></div>
    </div>

    </div>
</section>
    </div>
    `,

    data() {
        return {
            
        }
    },
    mounted() {

var stepIcon = document.querySelector('#steps2');

    var waypoint = new Waypoint({
        element: stepIcon,
        handler: function(direction) {
          console.log('Scrolled to waypoint!!!234!!');
          runAnimationK(this.element, ["stepA"], ["stepB"],["stepC"],["stepD"],["stepE"]);
          this.destroy();
        },offset: 150
      });



function runAnimationK(parent, elements, elementsA, elementsB, elementsC, elementsD){
  //should run on a waypoint or user interaction
  console.log("run animations here");

  let innerSVG = parent.contentDocument;


  //set up animation properties

  let animPropsK = {};
  let animPropsI = {};
  let animPropsL = {};
  let animPropsM = {};
  let animPropsN = {};
  switch (parent.id) {
      case "steps2":
      animPropsK = {y:-1000, opacity:0};
      animPropsI = {y:-1000, opacity:0, delay:.5};
      animPropsL = {y:-1000, opacity:0, delay:1};
      animPropsM = {y:-1000, opacity:0, delay:1.5};
      animPropsN = {y:-1000, opacity:0, delay:2};
      break;

      default:
      //do nothing
      break;
  }

  //simple vector animation with Greensock
  elements.forEach(item => {
      let target = innerSVG.querySelector(`#${item}`);
      TweenMax.from(target, .4, animPropsK);
  });

  elementsA.forEach(item => {
      let target = innerSVG.querySelector(`#${item}`);
      TweenMax.from(target, .4, animPropsI);
  });

  elementsB.forEach(item => {
      let target = innerSVG.querySelector(`#${item}`);
      TweenMax.from(target, .4, animPropsL);
  });

  elementsC.forEach(item => {
      let target = innerSVG.querySelector(`#${item}`);
      TweenMax.from(target, .4, animPropsM);
  });

  elementsD.forEach(item => {
      let target = innerSVG.querySelector(`#${item}`);
      TweenMax.from(target, .4, animPropsN);
  });

}

// curator.io get the SNS feed
(function(){
    var i, e, d = document, s = "script";i = d.createElement("script");i.async = 1;
    i.src = "https://cdn.curator.io/published/77add02a-a308-4ffa-9fe6-1f7ab88a7d19.js";
    e = d.getElementsByTagName(s)[0];e.parentNode.insertBefore(i, e);
    })();

    },
    methods: {    

    },
    components: {
    }
}