
export default {
    props: ['currentArticle'],
    template: `
        <div class="col-12 col-sm-6 col-md-4">
        <div class="example-2 card">
   
        <router-link :to="{ name: 'newsarticle', params:{ articleDetails: currentArticle}}">
                <div class="wrapper">
                    <img class="tinted" :src="'images/article/' + currentArticle.image" alt="organ donation article img">
                <div class="data">
                    <div class="content">
                    <span class="author">{{currentArticle.author}}</span>
                    <h1 class="title">{{currentArticle.title}}</h1>
                    <p class="text">{{currentArticle.subtitle}}</p>
                    <a href="" class="button">Read more</a>
                    <ul class="like">
                        <li><a href="#"><i class="fas fa-heart"></i></a></li> 
                        <li><a href="#"><i class="fas fa-thumbs-up"></i></a></li> 
                    </ul>
                    </div>
                </div>
                </div>
                </router-link>
      
            </div>
        </div>

    
     `,
     created(){
         console.log(this.currentArticle);
     },
     methods: {
   
    }

}