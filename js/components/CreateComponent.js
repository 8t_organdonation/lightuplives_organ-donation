export default {
    template: `
    <div>
    <div class="login-page">
    
    
    <div class="form">
    <p id="logTitle">CREATE USER</p>
  
    <form class="login-form" method="post" enctype="multipart/form-data">
        <label name="username" for="username" class="hide">Username</label>
        <input v-model="input.username" name="username" type="text" placeholder="username" />
        <label name="password" for="password" class="hide">Password</label>
        <input v-model="input.password" name="password" type="password" placeholder="password" />
        <label name="email" for="email" class="hide">Email</label>
        <input v-model="input.email" name="email" type="email" placeholder="email" />

        <label for="profilepic">(option)Profile Image:</label>
        <input v-on:change="profileAdd" ref="profile" type="file" name="profile" id="profile" />
        <button type="submit" @click.prevent="create_user()">Create User</button>
        
    </form>
    
    </div>
</div>
</div>
     `,

     data() {
         return {
             input: {
                firstname: "",
                 username: "",
                 password: "",
                 email: "",
                 profile: ""
             
             },
             loginMessage: "User Created! Now Login and Enjoy!"
          
         }
     },
 
     methods: {
      profileAdd(){
        this.input.profile = this.$refs.profile.files[0]
      },
        create_user() {
     
            // console.log(this.input.profile);
            

            if (this.input.username !== "" && this.input.password && this.input.email !== "") {
            //   do fetch call
            // add to form data
              let formData = new FormData();
      
              formData.append("username", this.input.username);
              formData.append("password", this.input.password);
              formData.append("email", this.input.email);
              formData.append("profile", this.input.profile);
              
             
              let url = (this.input.profile == "") ? `./admin/scripts/admin_createuser.php?empty`:`./admin/scripts/admin_createuser.php`;
            
              fetch(url, {
                method: "POST",
                body: formData
              })
                .then(res => res.json())
                .then(data => {
                  console.log(data);
                  if (data == "User Create Failed") {
                    console.log("Creation failed, try again");
                    this.$emit("autherror", data);  
                } else {
                  this.$router.push({ name: "login", params: { loginMessage: this.loginMessage } });
                  }
                })
                .catch(function (error) {
                  console.log(error);
                });
            } else {
              let warn = ("Fields shouldn't be blank");
              this.$emit("autherror", warn);  
            }
          }
    }
 }