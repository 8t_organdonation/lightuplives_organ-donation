export default {
    props: ['commentDetail'],
    template: `
<div class="media">
        <img class="mr-3" :src="'images/user_avatar/' + commentDetail.avatar" alt="Generic placeholder image">
        <div class="media-body">
            <span class="h5 my-0 d-block">{{commentDetail.user}}</span>
            <small class="text-muted mb-2 d-block">{{commentDetail.date}}</small>
            <p class="mb-0">
            {{commentDetail.content}}
            </p>
            <hr class="mt-2">
        </div>
</div>
    `,
  
  data() {
    return {
    }
  },

  methods: {
  }

}