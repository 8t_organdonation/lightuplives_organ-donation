import SingleCommentComponent from './SingleCommentComponent.js';

export default {
    props: ['articleDetails'],
    template: `
<div>
<div class="container-fluid">
<div class="row">
    <div class="col-12 col-sm-6 col-md-8 mx-auto">
        <h2 class="text-center">{{articleDetails.title}}</h2>
        <hr class="text-center titleHr">
    </div>           
    <div class="col-12">
        <p style="color:grey" class="text-center">{{articleDetails.subtitle}}</p>   
    </div>
</div>


</div>

<div id="article-header" class="col-8 mx-auto">
            <img :src="'images/article/' + articleDetails.image" alt="organ_donation_article">
        </div>

<div class="container-fluid">

<div class="row content">
    <div class="col-12 col-sm-6 col-md-8 mx-auto" style="margin-top:2rem">
        <p id="contentOne" class="text-left">{{articleDetails.content}}</p>
        <p id="source">Source:<i class="fas fa-arrow-right"></i><span id="links">{{articleDetails.source}}</span></p>
    </div>
</div>

<div class="col-12 col-sm-6 col-md-9 mx-auto">
<h2 class="text-left" style="color:grey">Comments</h2>
<div class="card">
<div class="card-body">

<singlecommentcomponent v-for="(comment, index) in commentList" :commentDetail="comment" :key="index"></singlecommentcomponent>     

            <div v-if="this.login =='true'">
            <div class="media">
              <img class="mr-3" :src="'images/user_avatar/' + this.currentUserImg" alt="Generic placeholder image">
              <div class="media-body">
                    <textarea v-model="input.comment" name="comment" id="commentSec" type="text" rows="4" placeholder="Leave Your comments Here!"></textarea>
                <hr class="mt-2">
              </div>
            </div>
            <button @click.prevent="create_comment()" class="commentBtn" type="submit" value="Send">Comment</button>
            </div>
            
            <div v-else>
            <div class="media-body col-12">
            <router-link :to="{ name: 'login'}"><button class="commentBtn" type="submit">LOG IN</button></router-link>
            <router-link :to="{ name: 'create'}"> <button class="commentBtn" type="submit">CREATE</button></router-link>
            </div>
            </div>
          
</div>
</div>
</div>
</div>

</div>
    `,

  data() {
    return {
        input:{
            comment:""
        },
      commentList: [],
      login:"",
      currentUserImg:""

    }
  },

  created: function() {
    this.fetchAllComments();
    let currentUser = JSON.parse(localStorage.getItem("cachedUser"));
    this.currentUserImg = currentUser.avatar;
    this.login = localStorage.getItem("loginAuthenticated");
console.log(this.login);
  },

  methods: {
    create_comment() {
        let currentUser = JSON.parse(localStorage.getItem("cachedUser"));
        console.log(currentUser);
        if (this.input.comment !== "") {
          let formData = new FormData();
          formData.append("comment", this.input.comment);    
          let url = `./admin/scripts/admin_createcomment.php?userA=${currentUser.avatar}&&userN=${currentUser.username}&&articleId=${this.articleDetails.id}`;
        
          fetch(url, {
            method: "POST",
            body: formData
          })
            .then(res => res.json())
            .then(data => {
              console.log(data);
              if (data == "Comment Create Failed") {
                console.log("Creation failed, try again");
                this.$emit("autherror", data);  
            } else {
                this.fetchAllComments();  
              }
            })
            .catch(function (error) {
              console.log(error);
            });
        } else {
          let warn = ("Fields shouldn't be blank");
          this.$emit("autherror", warn);  
        }
      },

    fetchAllComments() {
    let articleid = this.articleDetails.id;
    let url = `./admin/scripts/article.php?comment=${articleid}`;

      fetch(url)
        .then(res => res.json())
        .then(data => {this.commentList = data;
          console.log(data);
      
        })
      .catch(function(error) {
        console.error(error);
      });
    }
  },

components: {

    singlecommentcomponent: SingleCommentComponent
  }

}