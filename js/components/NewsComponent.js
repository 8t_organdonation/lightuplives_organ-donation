import SingleNewsComponent from './SingleNewsComponent.js';

export default {

    template: `

   <div>
   <div class="container-fluid">
   <div class="row">
       <div class="col-12 col-sm-6 col-md-4 text-left">
      
           <h2 id="articleTag">Article Page  <router-link :to="{ name: 'createarticle'}"><a v-if="this.adminAuth=='true'" href="#"><span id="addArt">Add New Article<i class="fas fa-plus-circle"></i></span></a> </router-link></h2>
       
        </div>
   </div>
   </div>


   <!-- Article Card Start From Here -->
   <div class="container-fluid article-card">
   <div class="row">

   <singlenewscomponent v-for="(article, index) in articleList" :currentArticle="article" :key="index"></singlenewscomponent>     
      
       </div>
   </div>

   </div> 
   


    `,

    data() {
        return {
            adminAuth:""
        }
    },  
  
created: function() {
    this.fetchAllArticles();
    this.adminAuth = localStorage.getItem("adminAuthenticated");
  },

  data() {
    return {
      articleList: []
    }
  },

  methods: {
  
    fetchAllArticles() {
      let url = `./admin/scripts/article.php?allarticle`;

      fetch(url)
        .then(res => res.json())
        .then(data => {this.articleList = data;
        //   console.log(data);
      
        })
      .catch(function(error) {
        console.error(error);
      });
    }
  },

components: {

    singlenewscomponent: SingleNewsComponent
  }
}