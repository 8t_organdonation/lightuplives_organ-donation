// TweenMax for Heart Logo
var mainFact = document.querySelector('#mainFact');
var logoMain = document.querySelector('#logoMain');

var waypoint = new Waypoint({
  element: mainFact,
  handler: function(direction) {


     TweenMax.from(logoMain, 0.375, {
      repeat: 6,
      yoyo: true,
      ease: Power1.easeIn,
      scale:1.1
    });
  
      this.destroy();
  },
  offset: 130
});


// parallax scroll
function parallax() {
  var $slider = document.querySelector("#s1Parallax1");
  var $slider2 = document.querySelector("#s2Parallax1");

	var yPos = window.pageYOffset / $slider.dataset.speed;
  yPos = -yPos;
  
  var yPos2 = window.pageYOffset / $slider2.dataset.speed;
	yPos = -yPos;
	
  var coords = '0% '+ yPos + 'px';
  var coords2 = '0% '+ yPos2 + 'px';
	
  $slider.style.backgroundPosition = coords;
  $slider2.style.backgroundPosition = coords2;
}

window.addEventListener("scroll", function(){
	parallax();	
});


// check answer function
var answers = document.querySelectorAll(".factAnswer");
var button = document.querySelectorAll(".checkBtn");

for(var i=0; i<button.length; i++){
  button[i].addEventListener('click', function(e) {
    var answer = answers[e.target.id];
    if (answer.getAttribute("data-text-swap") == answer.innerHTML) {
      answer.innerHTML = answer.getAttribute("data-text-original");
    } else {
      answer.setAttribute("data-text-original", answer.innerHTML);
      answer.innerHTML = answer.getAttribute("data-text-swap");
    }
  }, false);
}


  // number counter

const ready = (selector, callback) => {
	window.addEventListener('DOMContentLoaded', function () {
		const elems = [...document.querySelectorAll(selector)];
		if (elems.length) {
			for (let elem of elems) {
				callback(elem);
			}
		}
	});
};

ready('.counter', (stat) => {
	// pattern used to seperate input number from html into an array of numbers and non numbers.
	const patt = /(\D+)?(\d+)(\D+)?(\d+)?(\D+)?/;
	const time = 1000;
	let result = [...patt.exec(stat.textContent)];
	let fresh = true;
	let ticks;
	
	// Remove first full match from result array 
	result.shift();
	// Remove undefined values from result array 
	result = result.filter(res => res != null);

	while (stat.firstChild) {
		stat.removeChild(stat.firstChild);
	}

	for (let res of result) {
		if (isNaN(res)) {
			stat.insertAdjacentHTML('beforeend', `<span>${res}</span>`);
		}
		else {
			for (let i = 0; i < res.length; i++) {
				stat.insertAdjacentHTML('beforeend',
					`<span data-value="${res[i]}">
						<span>&ndash;</span>
						${Array(parseInt(res[i]) + 1).join(0).split(0).map((x, j) => `
							<span>${j}</span>
						`).join('')}
					</span>`
				);
			}
		}
	}


	ticks = [...stat.querySelectorAll('span[data-value]')];

	let activate = () => {
		let top = stat.getBoundingClientRect().top;
		let offset = (window.innerHeight * 3 / 4);

		setTimeout(() => {
			fresh = false;
		}, time);

		if (top < offset) {
			setTimeout(() => {
				for (let tick of ticks) {
					let dist = parseInt(tick.getAttribute('data-value')) + 1;
					tick.style.transform = `translateY(-${(dist) * 100}%)`
				}
			}, fresh ? time : 0);
			window.removeEventListener('scroll', activate);
		}
	}
	window.addEventListener('scroll', activate);
	activate();
});


