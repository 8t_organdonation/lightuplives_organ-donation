import MainComponent from './components/MainComponent.js';
import ContactComponent from './components/ContactComponent.js';
import NewsComponent from './components/NewsComponent.js';
import NewsArticlesComponent from './components/NewsArticlesComponent.js';
import StatisticsComponent from './components/StatisticsComponent.js';
import CreateComponent from './components/CreateComponent.js';
import CreateArticleComponent from './components/CreateArticleComponent.js';
import LoginComponent from './components/LoginComponent.js';
let router = new VueRouter({

  routes: [
      { path: '/', redirect: { name: "main"} },
      { path: '/home', redirect: { name: "main"} },
      { path: '/main', name: "main", component: MainComponent},
      { path: '/contact', name: "contact", component: ContactComponent},
      { path: '/news', name: "news", component: NewsComponent},
      { path: '/newsarticle', name: "newsarticle", component: NewsArticlesComponent, props: true},
      { path: '/statistics', name: "statistics", component: StatisticsComponent},
      { path: '/create', name: "create", component: CreateComponent},
      { path: '/createarticle', name: "createarticle", component: CreateArticleComponent,beforeEnter: (to, from, next) => {
        if (localStorage.getItem("adminAuthenticated")=="true") {
          next();
        } else {
          next("/login");
        }
     
      }},
      { path: '/login', name: "login", component: LoginComponent, props: true}
  ],scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
});

const vm = new Vue({
 
  data: {
    authenticated: false,
    toastmessage: "Login failed!"
  },
  created: function() {
    if (localStorage.getItem("loginAuthenticated")=="true") {
      this.authenticated = true;
    }    
  },


  methods: {
    addClassMenu(){
      var body = document.querySelector('body');
      body.classList.toggle('overlay-menu-open');
    },
    removeClassMenu(){
      var body = document.querySelector('body');
      body.classList.remove('overlay-menu-open');
    },
    logout() {
    // delete local session
    localStorage.setItem("loginAuthenticated", false);
    localStorage.setItem("adminAuthenticated", false);
    localStorage.setItem("cachedUser", false); 
    this.authenticated= false;

    // push user back to login page
    this.$router.push({ path: "/main" });

    },
    setAuthenticated(status) {
      if(status){
        this.authenticated = status;
      }

    },
    to_login(){
      this.$router.push({ path: "/login" });
    },

    popError(errorMsg) {
      // set the error message string and show the toast notification
      this.toastmessage = errorMsg;
      $('.toast').toast('show');
    }
  },

  router: router
}).$mount("#app");


router.beforeEach((to, from, next) => {
  var body = document.querySelector('body');
// console.log(to);
// console.log(vm.$route.path);
  if(to.name=='contact'){
    body.classList.add('yellow');
}else{
  body.classList.remove('yellow');
}


next();
  });