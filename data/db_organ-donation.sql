-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 26, 2019 at 08:47 AM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_organ-donation`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_article`
--

DROP TABLE IF EXISTS `tbl_article`;
CREATE TABLE IF NOT EXISTS `tbl_article` (
  `article_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `article_title` varchar(250) NOT NULL,
  `article_subTitle` varchar(250) NOT NULL,
  `article_image` varchar(20) NOT NULL,
  `article_content` text NOT NULL,
  `article_source` varchar(250) NOT NULL,
  `article_author` varchar(20) NOT NULL,
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_article`
--

INSERT INTO `tbl_article` (`article_id`, `article_title`, `article_subTitle`, `article_image`, `article_content`, `article_source`, `article_author`) VALUES
(1, 'Opinion: 242 reasons Canada should reform organ-donation laws', 'As a nurse, I strongly suggest moving toward presumed consent laws in Canada to meet the complex health needs of our population.', 'organ_article1.jpg', 'As a nurse specializing in transplant surgery, I have the privilege of accompanying patients and their families through the complex, scary and uncertain process of waiting for a new organ. I bear witness to the devastation when that organ does not come in time, but also rejoice alongside them when they receive a second chance at life.\r\n\r\nA significant gap exists between the number of people fortunate enough to receive an organ and those still waiting on the list. In Canada, 2,979 transplants were performed in 2017, leaving behind 4,333 individuals whose futures remain uncertain, and unfortunately, 242 who did not survive the wait.\r\n\r\nHow do we explain this gap? Recent polls suggest the vast majority of Canadians are in favour of organ donation. In fact, 91 per cent indicate willingness to freely donate their organs upon their death, however, only half the population take the appropriate measures to ensure this is a possibility. Canada currently operates under explicit-consent legislation, also known as “opt-in organ donation.”. To be considered a possible donor, one must register online or sign the back of their health-care card, as well as discuss their wishes with their family. Clearly, there are systemic barriers in place preventing the overwhelming majority of Canadians from fulfilling their wishes to be possible organ donors at their time of death.', 'https://montrealgazette.com/opinion/opinion-242-reasons-canada-should-reform-organ-donation-laws', 'ALEXANDRA GLEZOS'),
(2, 'Deer Lake man relishing life as three-time organ donor recipient', 'DEER LAKE, N.L. — It’s often said that it only takes “that once” to realize the importance of something like organ donation.', 'organ_article2.jpg', 'Imagine three times.\r\n\r\nJoe Northcott of Deer Lake is a three-time organ recipient. He vows the medical procedure is a life-changing treatment, and believes those willing to be a donor are making the ultimate selfless act.\r\n\r\nCommittee members for the first Green Shirt Day event, from left, Fayth Schroeder, Glendins Kenny, and Sharon Chynn Anstey\r\nCommittee members for the first Green Shirt Day event, from left, Fayth Schroeder, Glendins Kenny, and Sharon Chynn Anstey\r\nAt the age of 18, Northcott contracted streptococcal pharyngitis — more commonly known as strep throat. It was believed the infection spread and affected his kidneys. As life continued, pretty much normally for close to 18 years, his kidneys worsened.\r\n\r\n“During that last year my energy was a drag,” the businessman said. “I would tire easily … I would drive to Corner Brook and have to rest. I would have to come home from work around 10 o’clock and rest. I would go back to work, come home for lunch and have a rest. I would go home again around 3 or 4 o’clock to have another rest and go back to work. That’s how I was doing it for months.”\r\n\r\nEventually, his kidneys gave out. In 1980, his younger brother Jim would step up to donate a kidney.\r\n\r\n“To be honest, it wasn’t discussed that much,” he said. “I was just following on normally until the last year, when I started to get dragged out. He came forward to give me his kidney … What can I say? We were close, so you do what you can for your family.”\r\n\r\nThe transplant was done at Toronto Western.\r\n\r\nIn 1999, with little-to-no explanation, his liver faltered. Northcott says doctors ruled out liver problems associated with such things as alcohol or hepatitis, but could only say it was possibly linked to medication he had taken over the years.\r\n\r\nRegardless of the cause, he was in need of another organ transplant. This time, he received a liver from a cadaver. He does not know the person who died so he could continue to live, but he certainly knows how he feels about the family of that person.\r\n\r\n“I do think about how good the family was,” he said. “It must have been a good person who donated, and it goes back to the family too for the final decision. Whoever they were, were really good people.”', 'https://www.thewesternstar.com/living/deer-lake-man-relishing-life-as-three-time-organ-donor-recipient-294748/', 'Cory Hurley '),
(3, 'Families more likely to honour organ donor wishes with registry: Sask. health minister', 'Hopes new registry will improve low Sask. organ donation rate', 'organ_article3.jpg', 'Saskatchewan families, recipients and donors are hopeful a new organ donor registry will help encourage families to honour the wishes of their loved ones when they die.\r\n\r\nThe province announced Wednesday it will spend $558,000 to create the new registry as part of the provincial budget.\r\n\"So if you can make your wishes known on an online registry and your family can truly know what you wanted done with your organs when you pass away, it\'s an exciting step forward.\"\r\n\r\nWillenborg, who waited more than five years for a kidney transplant, hopes the new system will help reduce the transplant wait list.\r\n\r\nRegistries successful in other locations: Minister\r\nCurrently the system asks Saskatchewan residents to put a red sticker on their health card to indicate if they want to be a donor.\r\n\r\nThe registry won\'t change the fact that families can go against those wishes when the person has passed away.\r\n\r\nBut Health Minister Jim Reiter said there is data from other locations to suggest registries have better success rates when it comes to dealing with families.\r\n\"I\'m told that registries that are in operation, that in those cases 90 percent of families accept what the wishes were. Those without registries [the number is] less than 50 percent,\" said Reiter at the legislature on Thursday.\r\n\r\nIn 2015, Saskatchewan had the lowest rate among all Canadian provinces for organ donation from deceased donors.\r\n\r\nDr. Joanne Kawchuk is a donor physician with the Saskatchewan Health Authority. Her role, which was created in 2018, supports the development of a new organ donation program and offers advice on supporting families through the process.\r\n\r\nShe said there is evidence that registries help families feel more comfortable in allowing their loved one to become a donor.', 'https://www.cbc.ca/news/canada/saskatoon/saskatchewan-organ-donation-registry-reaction-1.5066995', 'Alicia Bridges'),
(4, 'Organ donation pilot project could increase N.B. donations by 30 per cent: health officials', 'WATCH: A new program in Moncton is testing out a way to increase the number of available organs by introducing an entirely new group of potential donors.', 'organ_article4.jpg', 'A new program in Moncton is testing out a way to increase organ donations by creating an entirely new group of potential donors.\r\n\r\nThe pilot project out of the Dr-Georges-L.-Dumont University Hospital Centre will allow physicians to retrieve organ donations after cardiovascular deaths (DCD), with consent from families.\r\n\r\nCurrently, four hospitals in the province can collect organs from donors, but only following neurological deaths.\r\nAt an announcement at the hospital Thursday, Dr. Rémi LeBlanc, chief of intensive care services at Georges Dumont, said provinces like B.C., Quebec and Ontario have been using the practice for the past decade.\r\n\r\nIt’s estimated New Brunswick could see an increase in donations by 20 to 30 per cent, based on numbers from those provinces.\r\n\r\nThe Georges Dumont will be the only hospital to offer the service during the pilot, but there’s possibility to expand to others.\r\n\r\nRiverview resident Dan McLaughlin has been waiting for a kidney for about two and-a half years.\r\n\r\nHe put a decal on his car, sharing his call for help in hopes of finding a match.\r\n\r\n“By six o’clock in the evening, I’m wiped,” he says. “I certainly don’t have the energy to do the things that most people like to do.”\r\n\r\nBut Thursday’s announcement gave him a sense of hope.\r\n\r\n“The more we can increase the rate of transplantation, the better,” he said.', 'https://globalnews.ca/news/5009564/organ-donation-pilot-project-nb/', 'Callum Smith');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_coment_article`
--

DROP TABLE IF EXISTS `tbl_coment_article`;
CREATE TABLE IF NOT EXISTS `tbl_coment_article` (
  `comment_article_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_id` mediumint(9) NOT NULL,
  `article_id` mediumint(9) NOT NULL,
  PRIMARY KEY (`comment_article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_coment_article`
--

INSERT INTO `tbl_coment_article` (`comment_article_id`, `comment_id`, `article_id`) VALUES
(1, 1, 1),
(2, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comment`
--

DROP TABLE IF EXISTS `tbl_comment`;
CREATE TABLE IF NOT EXISTS `tbl_comment` (
  `comment_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_user` varchar(250) NOT NULL,
  `comment_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `comment_content` text NOT NULL,
  `comment_avatar` varchar(250) NOT NULL,
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_comment`
--

INSERT INTO `tbl_comment` (`comment_id`, `comment_user`, `comment_date`, `comment_content`, `comment_avatar`) VALUES
(1, 'admin1', '2019-03-26 04:31:41', 'comment test1', 'default.jpg'),
(2, 'user1', '2019-03-26 06:04:21', 'comment test2', 'default.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_name` varchar(250) NOT NULL,
  `user_pass` varchar(250) NOT NULL,
  `user_email` varchar(250) NOT NULL,
  `user_avatar` varchar(50) NOT NULL DEFAULT 'default.jpg',
  `user_admin` varchar(10) NOT NULL DEFAULT 'no',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_name`, `user_pass`, `user_email`, `user_avatar`, `user_admin`) VALUES
(1, 'admin1', '1', 'admin@admin', 'default.jpg', 'yes'),
(2, 'user1', '1', 'user@1', 'default.jpg', 'no');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
