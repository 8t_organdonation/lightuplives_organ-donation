<?php 
    require_once('config.php');
    
        $title = trim($_POST['title']);
        $subtitle = trim($_POST['subtitle']);
        $articleImg = $_FILES['articleImg'];
        $content = trim($_POST['content']);
        $source = trim($_POST['source']);
        $author = trim($_POST['author']);
     
        //Validation
     
        if(empty($title) || empty($subtitle)|| empty($articleImg) || empty($content) || empty($source) || empty($author)){
                $result = 'Please fill the required fields!';
            }else{
            $tbl = "tbl_article";
         $result = createArticle($tbl,$title,$subtitle,$articleImg,$content,$source,$author);
            }
        
    	echo json_encode($result);

?>
