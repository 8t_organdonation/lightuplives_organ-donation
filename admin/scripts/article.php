<?php 

    if (isset($_GET['allarticle'])) {
        include('connect.php');
       
        $query = 'SELECT * from tbl_article';
   
        $getAllAricles = $pdo->prepare($query);
        $getAllAricles->execute();

        $articles = array();

        while($article = $getAllAricles->fetch(PDO::FETCH_ASSOC)) {
            $currentarticle =  array();
            $currentarticle['id'] = $article['article_id'];
            $currentarticle['title'] = $article['article_title'];
            $currentarticle['subtitle'] = $article['article_subTitle'];
            $currentarticle['image'] = $article['article_image'];
            $currentarticle['author'] = $article['article_author'];
            $currentarticle['source'] = $article['article_source'];
            $currentarticle['content'] = $article['article_content'];
            $articles[] = $currentarticle;
        }

        echo json_encode($articles);
    }

    if (isset($_GET['comment'])) {
        include('connect.php');
        $c_id = $_GET['comment'];
        $query = 'SELECT tbl_comment.comment_id, tbl_comment.comment_user, tbl_comment.comment_date, tbl_comment.comment_content, tbl_comment.comment_avatar FROM tbl_comment, tbl_article, tbl_coment_article WHERE tbl_comment.comment_id = tbl_coment_article.comment_id AND tbl_article.article_id = tbl_coment_article.article_id AND tbl_article.article_id = '.$c_id;
   
        $getAllComments = $pdo->prepare($query);
        $getAllComments->execute();

        $comments = array();

        while($comment = $getAllComments->fetch(PDO::FETCH_ASSOC)) {
            $currentcomment =  array();
            $currentcomment['id'] = $comment['comment_id'];
            $currentcomment['user'] = $comment['comment_user'];
            $currentcomment['date'] = $comment['comment_date'];
            $currentcomment['content'] = $comment['comment_content'];
            $currentcomment['avatar'] = $comment['comment_avatar'];
            $comments[] = $currentcomment;
        }

        echo json_encode($comments);
    }

  