<?php

function login($username, $password){

require_once('connect.php');
  //check if username exists
  $check_exist_query = "SELECT COUNT(*) FROM tbl_user WHERE user_name = :username";
  
  // var_dump($check_exist_query);
  // $user_set = $pdo->query($check_exist_query);
  $user_set = $pdo->prepare($check_exist_query);
  $user_set->execute(
    array(
      ':username' => $username
    )
  );
    // var_dump($user_set);exit;
  if ($user_set->fetchColumn() > 0) {
    $get_user_query = "SELECT * FROM tbl_user WHERE user_pass = :psw AND user_name = :username";
  
    $get_user_set = $pdo->prepare($get_user_query);
    $get_user_set->execute(
      array(
        ":psw" => $password,
        ":username" => $username
      )
    );

    while ($found_user = $get_user_set->fetch(PDO::FETCH_ASSOC)) {

        $user = array();

        $user['id'] = $found_user['user_id'];
        $user['username'] = $found_user['user_name'];
        $user['avatar'] = $found_user['user_avatar'];
        $user['admin'] = $found_user['user_admin'];
 
      // add any other non-sensitive details here...

      return $user;
    }

    if (empty($id)){
        $message = 'Check Your Password!';
        return $message;
    }

  } else {
    $message = 'No User Found';
    return $message;
  }
}