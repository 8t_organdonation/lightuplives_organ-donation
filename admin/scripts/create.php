<?php 
    function createUser($tbl,$username,$password,$email,$profile){
        include('connect.php');

        if($profile!== null){
          $file_type      = pathinfo($profile['name'], PATHINFO_EXTENSION);
          $accepted_types = array('gif', 'jpg', 'jpe', 'jpeg', 'png');
          if (!in_array($file_type, $accepted_types)) {
              throw new Exception('Wrong file types!');
          }
          // Check file size
          if ($profile["size"] > 500000) {
            echo "Sorry, your file is too large.";
            throw new Exception('Sorry, your file is too large!');
          }
          //3. Move the uploaded file around
          $sourceProperties = getimagesize($profile['tmp_name']);
          $fileNewName = time();
          $folderPath = '../../images/user_avatar/';
          $imageType = $sourceProperties[2];
  
  
          function imageResize($imageResourceId,$width,$height) {
            $targetWidth =50;
            $targetHeight =50;
            $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
            imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);
            return $targetLayer;
        }
          switch ($imageType) {
              case IMAGETYPE_PNG:
                  $imageResourceId = imagecreatefrompng($profile['tmp_name']); 
                  $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                  imagepng($targetLayer,$folderPath. $fileNewName. "_thumbnail.". $file_type);
                  break;
              case IMAGETYPE_GIF:
                  $imageResourceId = imagecreatefromgif($profile['tmp_name']); 
                  $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                  imagegif($targetLayer,$folderPath. $fileNewName. "_thumbnail.". $file_type);
                  break;
              case IMAGETYPE_JPEG:
                  $imageResourceId = imagecreatefromjpeg($profile['tmp_name']); 
                  $targetLayer = imageResize($imageResourceId,$sourceProperties[0],$sourceProperties[1]);
                  imagejpeg($targetLayer,$folderPath. $fileNewName. "_thumbnail.". $file_type);
                  break;
              default:
                  echo "Invalid Image type.";
                  exit;
                  break;
          }
          move_uploaded_file($profile['tmp_name'], $folderPath. $fileNewName. ".". $file_type);
   
        }

          //Insert Query  
          $create_user_query = 'INSERT INTO '.$tbl.'(user_name,user_pass,user_email, user_avatar)';
          $create_user_query .= ' VALUES(:username,:password,:email,:profile)';
          $create_user_set = $pdo->prepare($create_user_query);
          if($profile!== null){
          $create_user_set->execute(
              array(
                  ':username'=>$username,
                  ':password'=>$password,
                  ':email'=>$email,
                  ':profile'=>$fileNewName."_thumbnail.".$file_type
              )
              );
        }else{
            $create_user_set->execute(
              array(
                  ':username'=>$username,
                  ':password'=>$password,
                  ':email'=>$email,
                  ':profile'=>"default.jpg"
              )
            );
            }

        if($create_user_set->rowCount()){
          $message = 'User Created';
    
          return $message;
        }else{
          $message = 'User Create Failed';
          return $message;
        }

    }


    function createArticle($tbl,$title,$subtitle,$articleImg,$content,$source,$author){
    include('connect.php');
    
    $file_type      = pathinfo($articleImg['name'], PATHINFO_EXTENSION);
    $accepted_types = array('gif', 'jpg', 'jpe', 'jpeg', 'png');
    if (!in_array($file_type, $accepted_types)) {
        throw new Exception('Wrong file types!');
    }
    $target_path = '../../images/article/' . $articleImg['name'];
    if (!move_uploaded_file($articleImg['tmp_name'], $target_path)) {
        throw new Exception('Failed to move uploaded file, check permission!');
    }
    //Insert Query  
        $create_article_query = 'INSERT INTO '.$tbl.'(article_title,article_subTitle,article_image, article_content,article_source,article_author)';
        $create_article_query .= ' VALUES(:title,:subtitle,:articleImg,:content,:source,:author)';
        $create_article_set = $pdo->prepare($create_article_query);
          $create_article_set->execute(
            array(
                ':title'=>$title,
                ':subtitle'=>$subtitle,
                ':articleImg'=>$articleImg['name'],
                ':content'=>$content,
                ':source'=>$source,
                ':author'=>$author
            )
            );

      if($create_article_set->rowCount()){
        $message = 'Article Created';
  
        return $message;
      }else{
        $message = 'Article Create Failed';
        return $message;
      }

  }

  function createComment($tbl,$comment,$user_name,$user_avatar,$article_id){
    include('connect.php');
    
    //Insert Query  
        $create_comment_query = 'INSERT INTO '.$tbl.'(comment_user,comment_content,comment_avatar)';
        $create_comment_query .= ' VALUES(:user,:comment,:avatar)';
        $create_comment_set = $pdo->prepare($create_comment_query);
          $create_comment_set->execute(
            array(
              ':user'=>$user_name,
                ':comment'=>$comment,
                ':avatar'=>$user_avatar
            )
            );
         $last_id = $pdo->lastInsertId();

         $update_comment_query = 'INSERT INTO tbl_coment_article(comment_id, article_id)';
         $update_comment_query .= ' VALUES(:comment_id, :article_id)';
         $update_comment = $pdo->prepare($update_comment_query);
         $update_comment->execute(
             array(
                 ':comment_id' => $last_id,
                 ':article_id'  => $article_id
             )
         );
      if($create_comment_set->rowCount() && $update_comment->rowCount()){
        $message = 'Comment Created';
  
        return $message;
      }else{
        $message = 'Comment Create Failed';
        return $message;
      }

  }




 


      