<?php 
	require_once('config.php');

	if (empty($_POST['username']) || empty($_POST['password'])){
		$message = 'No Username or Password';
	} else {
		$username = $_POST['username'];
		$password = $_POST['password'];
		$message = login($username, $password);
	}

	echo json_encode($message);
?>